Flask==2.0.1
celery==5.1.2
redis==3.5.3
flower==1.0.0
Flask-Migrate==3.1.0
Flask-SQLAlchemy==2.5.1
Flask-CeleryExt==0.3.4